from celery import chord

from ceex.chord_node_kill.celery import dadd
from ceex.shared.tasks import total_sum


chord_body = [dadd.s(n + 1, 9 - n, 20) for n in range(10)]
chord_cb = total_sum.s()
res = chord(chord_body)(chord_cb)
print(res.id)
tsum = res.get()
assert(tsum == 100)
