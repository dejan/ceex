ceex
====

**CE**lery **EX**periments

Run worker
----------

To run worker for particular experiment execute:

`runworker.sh [experiment name]`

If experiment name is omitted it runs the `chord_node_kill` experiment.

Configuration
-------------

To check worker configuration for particular experiment
run the following:

`celery -A ceex.<experiment name> inspect conf`

Example:

`celery -A ceex.chord_node_kill inspect conf`

Run tasks
---------

```bash
# activate virtual environment
. path/to/env/bin/activate
# in ceex root directory
python3 chord_node_kill.py
```

WIP
---

(Ignore this section)

Output of inspect conf from some of my Celery workers:
```
        "task_acks_late": true,
        "task_annotations": {
            "tasks.add": {
                "rate_limit": "10/s"
            }
        },
        "task_reject_on_worker_lost": "True",
        "task_send_sent_event": "True",
        "task_store_errors_even_if_ignored": true,
        "task_track_started": "True",
        "worker_pool_restarts": "True",
        "worker_prefetch_multiplier": 1,
        "worker_send_task_events": "True"
```
