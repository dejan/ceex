# SHARED Celery app
###################

from celery import Celery

app = Celery('ceex',
             broker='redis://localhost/13',
             backend='redis://localhost/14',
             include=['ceex.shared.tasks'])

app.conf.update(
    result_expires=3600,
)
