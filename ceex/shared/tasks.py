from celery.utils.log import get_task_logger
from .celery import app


LOGGER = get_task_logger(__name__)

# ceex.shared.tasks.add
#@app.task(name="ceex.add")
@app.task
def add(x, y):
    return x + y

# ceex.shared.tasks.mul
#@app.task(name="ceex.mul")
@app.task
def mul(x, y):
    return x * y

# ceex.shared.tasks.total_sum
#@app.task(name="ceex.total_sum")
@app.task
def total_sum(numbers):
    LOGGER.info(numbers)
    return sum(numbers)
