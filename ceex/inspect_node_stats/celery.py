from time import sleep
import os

from celery.worker.control import inspect_command
import psutil

import ceex.shared.celery as shared_celery


@inspect_command(
    args=[('disk_dir', str)],
    signature='[disk_dir="/"]',
)
def node_stats(state, disk_dir="/", **kwargs):  # pragma: no cover
    """Dump node statistics

    Parameters
    ----------
    state
        No idea what kind of object is this.
    disk_dir : str
        Mount point for disk stats. Defaults to "/".
    """

    # This, for some reason makes the worker crash...
    # _cpu_percent = psutil.cpu_percent()
    # So, I had to use a workaround:
    load1, load5, load15 = psutil.getloadavg()
    _cpu_percent = (load15/os.cpu_count()) * 100

    _mem_percent = psutil.virtual_memory()[2]
    _swap_percent = psutil.swap_memory()[3]

    if disk_dir in ["/", "/work"]:  # For now we only allow "/" (default), and "/work"
        _disk_percent = psutil.disk_usage(disk_dir)[3]
    else:
        _disk_percent = 0.0

    return {"cpu": _cpu_percent, "mem": _mem_percent, "swap": _swap_percent,
            f"disk{disk_dir}": _disk_percent}


app = shared_celery.app
app.conf.update(
    task_acks_late=True,
    task_reject_on_worker_lost=True,
    # We set extremely short visibility_timeout to test whether tasks get redelivered
    # after this period of time - 60 sec
    broker_transport_options={'visibility_timeout': 60}
)


# (name="ceex.dadd")
@app.task
def dadd(a: int, b: int = 0, sec: float = 1.0) -> int:
    sleep(sec)
    return a + b

