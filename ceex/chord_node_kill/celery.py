import ceex.shared.celery as shared_celery
from time import sleep


app = shared_celery.app

app.conf.update(
    task_acks_late=True,
    task_reject_on_worker_lost=True,
    # We set extremely short visibility_timeout to test whether tasks get redelivered
    # after this period of time - 60 sec
    broker_transport_options={'visibility_timeout': 60}
)

# (name="ceex.dadd")
@app.task
def dadd(a: int, b: int = 0, sec: float = 1.0) -> int:
    sleep(sec)
    return a + b
