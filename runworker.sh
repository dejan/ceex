#!/usr/bin/env bash

# If first parameter not given, we assume "chord_node_kill" experiment by default
experimentName=${1:-"chord_node_kill"}

# By default, if second parameter not given, we create one worker
workerID=${2:-1}

bold=$(tput bold)
purple='\033[1;35m'
yellow='\033[1;33m'
normal=$(tput sgr0)
celeryCommand="celery -A ceex.${experimentName} worker -n celery${workerID}@localhost -c 10 -O fair -l info"
echo -e "${bold}${purple}${celeryCommand}${normal}"
echo -e "${yellow}:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::${normal}"
$celeryCommand
