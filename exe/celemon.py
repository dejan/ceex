#! /usr/bin/env python3

# Python
from datetime import datetime
# 3rd party
from celery import Celery


CONFIG = {
    "app_name": "celery_app",
    "broker_url": "rediss://:5up3r53cr3t@localhost:6379/12?ssl_cert_reqs=CERT_OPTIONAL",
    "celery_result_backend": "rediss://:5up3r53cr3t@localhost:6379/14?ssl_cert_reqs=CERT_OPTIONAL",
    "celery_result_serializer": "msgpack",
    "celery_task_serializer": "msgpack"
}


def my_monitor(app):
    state = app.events.State()
    error_cnt = 0
    task_info = {}  # used by all handlers to cache some data

    def handle_task_received(event):
        state.event(event)
        uuid = event['uuid']
        name = event['name']
        args = event['args']
        isodt = datetime.utcnow().isoformat()
        print("R", isodt, uuid, name, args, flush=True)

    def handle_task_succeeded(event):
        # task-succeeded(uuid, result, runtime, hostname, timestamp)
        state.event(event)
        # task = state.tasks.get(event['uuid'])
        uuid = event['uuid']
        tstamp_val = datetime.utcfromtimestamp(event['timestamp'])
        data = {"uuid": event['uuid'],
                "result": event['result'],
                "runtime": event['runtime'],
                "hostname": event['hostname'],
                "tstamp": tstamp_val}
        isodt = datetime.utcnow().isoformat()
        print("#", isodt, uuid, flush=True)

    def handle_task_sent(event):
        # task-sent(uuid, name, args, kwargs, retries, eta, expires, queue, exchange,
        # routing_key, root_id, parent_id)
        state.event(event)
        nonlocal task_info
        uuid = event['uuid']
        name = event['name']
        args = event['args']
        isodt = datetime.utcnow().isoformat()
        print(">", isodt, uuid, name, flush=True)

    def handle_task_started(event):
        # task-started(uuid, hostname, timestamp, pid)
        nonlocal task_info
        state.event(event)

        uuid = event['uuid']
        isodt = datetime.utcnow().isoformat()
        print("S", isodt, uuid, flush=True)

    def handle_task_failed(event):
        # task-failed(uuid, exception, traceback, hostname, timestamp)
        state.event(event)
        nonlocal task_info
        # task name is sent only with -received event, and state
        # will keep track of this for us.
        uuid = event['uuid']
        excep = event["exception"]
        isodt = datetime.utcnow().isoformat()
        print("F", isodt, uuid, excep, flush=True)
        if "traceback" in event:
            trcbck = event["traceback"]
            if trcbck:
                print(trcbck)
                print("^" * 80)

    with app.connection() as connection:
        recv = app.events.Receiver(connection, handlers={
            'task-failed': handle_task_failed,
            'task-received': handle_task_received,
            'task-sent': handle_task_sent,
            'task-succeeded': handle_task_succeeded,
            'task-started': handle_task_started,
            '*': state.event,
        })
        while not recv.should_stop:
            recv.capture(limit=None, timeout=None, wakeup=True)


if __name__ == '__main__':
    app = Celery(CONFIG['app_name'])
    app.conf.update(**CONFIG)
    my_monitor(app)
